# Challange Chapter 4

### About This Project :

- Challange chapter 4
- Build some logic for this game
- Implementing OOP
- This project was created to fulfill the graduation requirements for chapter 4 of Binar Bootcamp Full-Stack Web
- The background of this traditional game is "suit" (rock, paper, scissors)

### Technology Used :

- WSL 2: Ubuntu-20.04
- Git
- HTML
- CSS
- Bootstrap 5
